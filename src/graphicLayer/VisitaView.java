package graphicLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import ava.DateLabelFormatter;
import model.database.PazientiTable;
import model.database.VisiteTable;
import utils.Pair;

public class VisitaView extends JFrame {


    /**
     * constructor. 
     */
    public VisitaView(String codicePsichiatra){
        this.codPsichiatra = Integer.valueOf(codicePsichiatra);
        initGUI();
    }

    Pair<Vector, Vector> tableData = null;
    private JDatePanelImpl datePanel;
    JDatePickerImpl datePicker;
    int codPsichiatra= -1;
    JTable table;

    public void initGUI() {
        tableData = VisiteTable.getTable().findAll();
        UtilDateModel model = new UtilDateModel();
        Properties prop = new Properties();             
        prop.put("text.today", "Today");
        prop.put("text.month", "Month");
        prop.put("text.year", "Year");
        datePanel = new JDatePanelImpl(model, prop);  
        datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        setTitle("");

        JPanel panel = new JPanel(new GridBagLayout());
        this.getContentPane().add(panel);

//        DefaultTableModel modello = new DefaultTableModel(tableData.getX(), 0);
//        modello.addRow(tableData.getY());
       
      
        table = new JTable(tableData.getY(),tableData.getX());

        JScrollPane tableScrollPane = new JScrollPane(table);
        tableScrollPane.setPreferredSize(new Dimension(200, 50));

        JLabel label = new JLabel("Visite");

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(new JButton("Visualizza Visite in questa Data"));

        JPanel detailsPanel = new JPanel();
        detailsPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        panel.add(label, gbc);


        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridx = 0;
        gbc.gridy = 1;          
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        panel.add(tableScrollPane, gbc);

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 0;
        gbc.weighty = 0;
        panel.add(buttonPanel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.gridheight = 2;
        gbc.anchor = GridBagConstraints.NORTH;

        panel.add(createDetailsPanel(), gbc);

        this.pack();
        this.setVisible(true);
    }

    private JPanel createDetailsPanel() {


        JPanel panel = new JPanel();

        JLabel codLabel = new JLabel("Codice Paziente");
        JLabel DataLabel = new JLabel("Data");


        JTextField thingNameField = new JTextField("");
        JTextField anAttributeField = new JTextField("");

        panel.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        int i=0;

        gbc.insets = new Insets(2,2,2,2);
        gbc.anchor = GridBagConstraints.NORTHEAST;

        gbc.gridx = 0;
        gbc.gridy = i;
        panel.add(codLabel,  gbc);

        JComboBox<String> comboPazienti = new JComboBox<>();
        gbc.gridx = 1;
        gbc.gridy = i;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;               
        panel.add(comboPazienti,  gbc);    
        String[] pazienti = PazientiTable.getTable().getPazienti(codPsichiatra);
        comboPazienti.setModel(new DefaultComboBoxModel<String>(pazienti));
        i++;

        gbc.gridx = 0;
        gbc.gridy = i;          
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        panel.add(DataLabel,  gbc);

        gbc.gridx = 1;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(datePicker,gbc);     

        i++;

        JButton aggiungiBotton = new JButton("Aggiungi Visita");
        gbc.gridx = 1;
        gbc.gridy = i;          
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(aggiungiBotton,  gbc);

        aggiungiBotton.addActionListener(new ActionListener() {           
            @Override
            public void actionPerformed(ActionEvent e) {
             
                VisiteTable.getTable().persist(Integer.valueOf((String)comboPazienti.getSelectedItem()), codPsichiatra, 
                       (String) datePicker.getJFormattedTextField().getText());
                updateTable();
                DefaultTableModel dm = (DefaultTableModel)table.getModel();
                dm.fireTableDataChanged(); // notifies the JTable that the model has changed
            }
        });

        return panel;
    }

    private void updateTable() {
        tableData = VisiteTable.getTable().findAll();
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        Vector row = (Vector) tableData.getY().get(tableData.getY().size()-1);
        model.addRow(row);        
    }
}

