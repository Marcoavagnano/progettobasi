package graphicLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import ava.DateLabelFormatter;
import model.database.IsolamentiTable;
import model.database.PazientiTable;
import model.database.StanzeTable;
import model.database.VisiteTable;
import utils.Pair;

public class IsolamentoView extends JFrame {


    /**
     * constructor. 
     */
    public IsolamentoView(String codicePsichiatra){
        this.codPsichiatra = Integer.valueOf(codicePsichiatra);
        initGUI();
    }

    Pair<Vector, Vector> tableData = null;
    private JDatePanelImpl datePanel;
    JDatePickerImpl datePicker;
    JDatePickerImpl dateFinePicker;
    int codPsichiatra= -1;
    JTable table;

    public void initGUI() {
        updateTable();
        UtilDateModel model = new UtilDateModel();
        Properties prop = new Properties();             
        prop.put("text.today", "Today");
        prop.put("text.month", "Month");
        prop.put("text.year", "Year");
        datePanel = new JDatePanelImpl(model, prop);  
        datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        dateFinePicker = new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter());
        setTitle("");

        JPanel panel = new JPanel(new GridBagLayout());
        this.getContentPane().add(panel);

        table = new JTable(tableData.getY(), tableData.getX());

        JScrollPane tableScrollPane = new JScrollPane(table);
        tableScrollPane.setPreferredSize(new Dimension(200, 50));

        JLabel label = new JLabel("Isolamento");

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(new JButton("Visualizza Isolamento in questa Data"));

        JPanel detailsPanel = new JPanel();
        detailsPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        panel.add(label, gbc);


        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridx = 0;
        gbc.gridy = 1;          
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        panel.add(tableScrollPane, gbc);

        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 0;
        gbc.weighty = 0;
        panel.add(buttonPanel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        gbc.gridheight = 2;
        gbc.anchor = GridBagConstraints.NORTH;

        panel.add(createDetailsPanel(), gbc);

        this.pack();
        this.setVisible(true);
    }

    private JPanel createDetailsPanel() {


        JPanel panel = new JPanel();

        JLabel codLabel = new JLabel("Codice Paziente");
        JLabel stanza = new JLabel("Stanza");
        JLabel livello = new JLabel("Livello");
        JLabel DataLabel = new JLabel("DataInizio");
        JLabel fineLabel = new JLabel("DataFine");


        JTextField livelloField = new JTextField("");
        JTextField anAttributeField = new JTextField("");

        JComboBox<String> comboStanze = new JComboBox<>(StanzeTable.getTable().getStanzeLibere());
        
        panel.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        int i=0;

        gbc.insets = new Insets(2,2,2,2);
        gbc.anchor = GridBagConstraints.NORTHEAST;

        gbc.gridx = 0;
        gbc.gridy = i;
        panel.add(codLabel,  gbc);

        JComboBox<String> comboPazienti = new JComboBox<>();
        gbc.gridx = 1;
        gbc.gridy = i;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;               
        panel.add(comboPazienti,  gbc);    
        String[] pazienti = PazientiTable.getTable().getPazienti(codPsichiatra);
        comboPazienti.setModel(new DefaultComboBoxModel<String>(pazienti));
        i++;

        gbc.gridx = 0;
        gbc.gridy = i;          
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        panel.add(DataLabel,  gbc);

        gbc.gridx = 1;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(datePicker,gbc);     

        i++;
        
        gbc.gridx = 0;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(fineLabel,gbc);     

        gbc.gridx = 1;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(dateFinePicker,gbc);     

        i++;
        
        gbc.gridx = 0;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(livello,gbc);     

        gbc.gridx = 1;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(livelloField,gbc);     

        i++;
        
        gbc.gridx = 0;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(stanza,gbc);     

        gbc.gridx = 1;
        gbc.gridy = i;          
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(comboStanze,gbc);     

        i++;

        JButton aggiungiBotton = new JButton("Aggiungi Isolamento");
        gbc.gridx = 1;
        gbc.gridy = i;          
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(aggiungiBotton,  gbc);

        aggiungiBotton.addActionListener(new ActionListener() {           
            @Override
            public void actionPerformed(ActionEvent e) {
             
                //String a = (String) datePicker.getModel().getValue();
                System.out.println(datePicker.getModel().getValue());
                System.out.println(datePicker.getJFormattedTextField().getValue());
//                VisiteTable.getTable().persist(Integer.valueOf((String)comboPazienti.getSelectedItem()), codPsichiatra, 
//                        new java.sql.Date(((java.util.Date)datePicker.getModel().getValue()).getTime()));
                updateTable();
                DefaultTableModel dm = (DefaultTableModel)table.getModel();
                dm.fireTableDataChanged(); // notifies the JTable that the model has changed
            }
        });

        return panel;
    }

    private void updateTable() {
        tableData = IsolamentiTable.getTable().findByPsychiatric(codPsichiatra);

    }
}

