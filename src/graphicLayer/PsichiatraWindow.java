package graphicLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class PsichiatraWindow extends JFrame{
    
        JLabel title, idLabel, nameLabel, dataLabel, addressLabel, contactLabel,provenienzaLabel;

        JTextField idField, nameField, genderField, addressField, contactField,provenienzaField;

        JButton visiteButton, exitButton;

        JRadioButton male, female;
        ButtonGroup bg;

        JPanel panel;
        JTable table;

        DefaultTableModel model;


        JScrollPane scrollpane;
        
        public PsichiatraWindow(Object obj) {
            // TODO Auto-generated constructor stub
            
            super("Psichiatra ID:" + String.valueOf(obj));
            setSize(770, 620);
            setLayout(null);

            // Defining Labels 
            String codPsichiatra = String.valueOf(obj);
            
            title = new JLabel("Paziente ID:" + codPsichiatra);

            title.setBounds(60, 7, 200, 30);

            idLabel = new JLabel("CF");

            idLabel.setBounds(30, 50, 60, 30);

            nameLabel = new JLabel("Nome"); 

            nameLabel.setBounds(30, 85, 60, 30);

            dataLabel = new JLabel("Cognome"); 

            dataLabel.setBounds(30, 120, 60, 30);

            addressLabel = new JLabel("DataDiNascita"); 

            addressLabel.setBounds(30, 155, 60, 30); 

            contactLabel = new JLabel("DataArrivo"); 

            contactLabel.setBounds(30, 190, 60, 30);

            provenienzaLabel = new JLabel("Provenienza"); 

            provenienzaLabel.setBounds(30, 190, 60, 30);

            // Defining ID field
            idField = new JTextField(); 

            idField.setBounds(95, 50, 130, 30);
            idField.setEnabled(false);


            // Defining Name field
            nameField = new JTextField(); 

            nameField.setBounds(95, 85, 130, 30);         


            // Defining Gender Buttons
            male = new JRadioButton("Male");

            male.setBounds(95, 120, 60, 30);


            female = new JRadioButton("Female");
            female.setBounds(155, 120, 70, 30);            


            bg = new ButtonGroup(); 
            bg.add(male); 
            bg.add(female); 
            addressField = new JTextField(); 
            addressField.setBounds(95, 155, 130, 30);
            contactField = new JTextField(); 
            contactField.setBounds(95, 190, 130, 30);
            provenienzaField = new JTextField(); 
            provenienzaField.setBounds(95, 235, 130, 30);

            // fixing all Label,TextField,RadioButton

            add(title);

            add(idLabel);

            add(nameLabel);

            add(dataLabel);

            add(addressLabel);

            add(contactLabel);
            add(provenienzaLabel);

            add(idField);

            add(nameField);

            add(male);

            add(female);

            add(addressField);

            add(contactField);

            add(provenienzaField);

            // Defining Exit Button

            exitButton = new JButton("Exit"); 

            exitButton.setBounds(25, 250, 80, 25);            


            // Defining Register Button

            visiteButton = new JButton("Visite");

            visiteButton.setBounds(110, 250, 100, 25);


            // fixing all Buttons

            add(exitButton);

            add(visiteButton);


            // Defining Panel

            panel = new JPanel();

            panel.setLayout(new GridLayout());

            panel.setBounds(250, 20, 480, 330);

            panel.setBorder(BorderFactory.createDashedBorder(Color.blue));

            add(panel);


            //Defining Model for table

            model = new DefaultTableModel();

            //Adding object of DefaultTableModel into JTable

            table = new JTable(model);

            // Enable Scrolling on table

            scrollpane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,

                    JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

            panel.add(scrollpane);



            //Displaying Frame in Center of the Screen

            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

            this.setLocation(dim.width/2-this.getSize().width/2,

                    dim.height/2-this.getSize().height/2);

            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            setResizable(false);

            setVisible(true);

        }

    }
