package graphicLayer;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoginPsyc extends JFrame{
	private String psychiatrist;
	public LoginPsyc( ) {
	    this.setDefaultCloseOperation(EXIT_ON_CLOSE);;
        this.setSize(400, 200);
        JPanel log=new JPanel();
        JLabel jL=new JLabel("Codice Fiscale:");
        
        JTextField jt=new JTextField();
        JButton jb=new JButton("Accedi");
        jt.setColumns(20);
        log.add(jL);
        log.add(jt);
        log.add(jb);
        this.getContentPane().add(log);
        this.setVisible(true);
        ActionListener loginAl = e -> {
        	psychiatrist=jt.getText();
        	notifyAll();
        	dispose();
        };
        jb.addActionListener(loginAl);
	}
	public String getPsychiatrist() {
		return psychiatrist;
	}
}
