package graphicLayer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import ava.DateLabelFormatter;
import connection.DBConnection;
import entity.Paziente;
import entity.Psichiatra;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Vector;

import model.database.AreeTable;
import model.database.AttrezzatureTable;
import model.database.GuardieTable;
import model.database.MedicinaliTable;
import model.database.PazientiTable;
import model.database.PsichiatriTable;
import model.database.StanzeTable;
import model.database.VisiteTable;
import utils.Pair;

public class MainWindow extends JFrame {
    String[] views = new String[] {"AMMINISTRATORE", "PSICHIATRA", "PAZIENTE" };
    private String[] entitiesName = new String[] { "PSICHIATRI", "GUARDIE", "MEDICINALI", "ATTREZZATURE",
    "STANZE" };
    String[] adminName = new String[] { "PSICHIATRI", "GUARDIE", "MEDICINALI", "ATTREZZATURE", "AREE", "STANZE"};
    String[] psycName = new String[] { "PAZIENTI", "VISITA","ISOLAMENTO","STANZE" };
    final String[] medAttributes=new String[] { "Codice Medicinale*", "Tipo*", "Data Scadenza*", "Descrizione*", "Numero Scorte*"};
    final String[] equipAttributes=new String[] { "Codice Attrezzatura*", "Descrizione*", "Costo*"};
    final String[] roomAttributes=new String[] { "Codice Stanza*", "Settore*","Posti Occupati*", "Codice Area*"};
    final String[] areaAttributes=new String[] { "Codice Area*", "Piano*"};
    DBConnection sqlDriver = new DBConnection();
    private static final long serialVersionUID = 1L;
    final Font labelFont = new Font("", Font.BOLD, 12);
    private GridBagConstraints personGrid = new GridBagConstraints();
    JComboBox<String> vList = new JComboBox<>(views);
    JLabel lb1 = new JLabel("Scegli la vista: ");
    JPanel topPanel = new JPanel(new GridLayout(2, 3));
    JPanel centralPanel = new JPanel(new GridLayout(2, 1));
    JPanel bottomPanel = new JPanel(new GridLayout(1, 2));
    JButton carryB = new JButton("Aggiungi al DB");
    JButton visB = new JButton("Tabella");
    JButton insB = new JButton("Inserisci");
    JLabel lbC = new JLabel("Scegli la tabella : ");
    JComboBox<String> eList = new JComboBox<String>();
    JComboBox<String> stanzeLibere=new JComboBox<String>(StanzeTable.getTable().getStanzeLibere());
    String entityChoose =" ";
    Properties prop;   
    JDatePanelImpl datePanel;
    JTable table;
    public MainWindow() {
        UtilDateModel model = new UtilDateModel();
        prop = new Properties(); 
        prop.put("text.today", "Today");
        prop.put("text.month", "Month");
        prop.put("text.year", "Year");
        datePanel = new JDatePanelImpl(model, prop);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(700, 700);
        bottomPanel.add(carryB);

        this.getContentPane().add(BorderLayout.NORTH, topPanel);
        this.getContentPane().add(centralPanel);
        this.getContentPane().add(BorderLayout.SOUTH, bottomPanel);
        topPanel.add(lb1);
        topPanel.add(vList);
        topPanel.add(lbC);
        topPanel.add(eList);
        //		topPanel.add(visB);
        //		topPanel.add(insB);
        ActionListener chooseViewAl = e -> {
            String key = views[vList.getSelectedIndex()];
            eList.removeAllItems();
            if (key.equals("AMMINISTRATORE")) {
                eList.setVisible(true);
                lbC.setVisible(true);
                for (String s : adminName) {
                    eList.addItem(s);
                }
            } else if(key.equals("PSICHIATRA")) {
                centralPanel.removeAll();
                centralPanel.add(login());
                eList.setVisible(false);
                lbC.setVisible(false);
                SwingUtilities.updateComponentTreeUI(this);
            }
            else {
                centralPanel.removeAll();
                centralPanel.add(loginPaziente());
                eList.setVisible(false);
                lbC.setVisible(false);
                SwingUtilities.updateComponentTreeUI(this);
            }
        };
        
        ActionListener setEntityAl = e -> {
            insertInto(entityChoose);
            updateCentralPanel();
            SwingUtilities.updateComponentTreeUI(this);
        };
        
        ActionListener visA = e -> {
            if(eList.getItemCount()!=0) {
                entityChoose = (String) eList.getSelectedItem();

                insB.setEnabled(true);
                updateCentralPanel();
                bottomPanel.remove(carryB);
                SwingUtilities.updateComponentTreeUI(this);

            }
        };
        ActionListener insA = e -> {

            entityChoose = (String) eList.getSelectedItem();

            visB.setEnabled(true);
            centralPanel.removeAll();
            centralPanel.add(selectGUI(entityChoose));
            bottomPanel.add(carryB);
            SwingUtilities.updateComponentTreeUI(this);

        };

        carryB.addActionListener(setEntityAl);
        eList.addActionListener(visA);
        insB.addActionListener(insA);
        vList.addActionListener(chooseViewAl);
        vList.setSelectedIndex(0);
        // eList.addActionListener(el);
        this.setVisible(true);
    }
    private JPanel loginPaziente() {
        final JPanel panel = new JPanel();
        final JLabel lblId = new JLabel("CodicePaziente");
        final JComboBox<String> comboPsichitri = new JComboBox<>(PazientiTable.getTable().getPazienti());

        lblId.setBounds(154, 90, 46, 14);


        final JButton btnLogin = new JButton("Login");

        btnLogin.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(() -> new PazienteFinestra(comboPsichitri.getSelectedItem()));
            }
        });

        comboPsichitri.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        btnLogin.setBounds(183, 146, 89, 23);
        panel.add(lblId);
        panel.add(comboPsichitri);
        panel.add(btnLogin);
        return panel;
    }
    
    private JPanel login() {
        final JPanel panel = new JPanel();
        final JLabel lblId = new JLabel("CodicePsichiatra");
        final JComboBox<String> comboPsichitri = new JComboBox<>(PsichiatriTable.getTable().getPsichiatri());

        lblId.setBounds(154, 90, 46, 14);


        final JButton btnLogin = new JButton("Login");

        btnLogin.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(() -> new PsichiatraFinestra(comboPsichitri.getSelectedItem()));
            }
        });

        comboPsichitri.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        btnLogin.setBounds(183, 146, 89, 23);
        panel.add(lblId);
        panel.add(comboPsichitri);
        panel.add(btnLogin);
        return panel;
    }
    private void updateCentralPanel() {
        centralPanel.removeAll();
        centralPanel.add(selectGUI(entityChoose));
        centralPanel.add(visualizeTable(entityChoose));
    }
    private List<String> getTextF(Container container) {
        List<String> temp = new ArrayList<>();
        for (Component comp : container.getComponents()) {
            if (comp instanceof JTextField) {
                temp.add(((JTextField) comp).getText());
            }
            else if(comp instanceof JDatePickerImpl) {
                temp.add(((JDatePickerImpl) comp).getJFormattedTextField().getText());
            }
        }
        return temp;
    }

    private JPanel selectGUI(final String choose) {
        switch (choose) {
        case "PSICHIATRI":
            return psycGUI();
        case "PAZIENTI":
            return patientGUI();
        case "GUARDIE":
            return guardGUI();
        case "MEDICINALI":
            return medicineGUI();
        case "ATTREZZATURE":
            return attrezzatureGUI();
        case "AREE":
            return AreeGUI(areaAttributes,1,2);
        case "STANZE":
            return defaultGUI(roomAttributes, 4,2);
        
        default:
            return new JPanel();
        }
    }

    private JPanel AreeGUI(String[] attributes, int rows, int col) {
        boolean evenRows;
        int index=0;
        
        JPanel medJP =new JPanel();
        GridBagConstraints medGrid = new GridBagConstraints();
        medJP.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        medJP.setLayout(new GridBagLayout());
        medGrid.fill = GridBagConstraints.HORIZONTAL;
        evenRows = isEven(attributes.length);
        for (int i = 0; i < rows; i++) {
            medGrid.gridy++;
            for (int j = 0; j < col; j++) {
                medGrid.gridx = j;
                medGrid.gridheight = 1;
                medGrid.gridwidth = 1;
                medGrid.weightx = 0.5;
                if (isEven(i) && index < attributes.length) {
                    JLabel jl = new JLabel("" + attributes[index]);
                    jl.setFont(labelFont);
                    medJP.add(jl, medGrid);
                    index++;
                } else {
                    if (!evenRows && j > 0 && index >= attributes.length) {
                        break;
                    }
                    medJP.add(new JTextField(), medGrid);
                }
            }
        }
        return medJP;
    }
    private JPanel personGUI() {
        int index = 0;
        final int col = 2;
        final int rows = 8;
        final boolean evenRows;
        boolean isDate=false;
        final String[] iLabelName = new String[] { "CF*: ", "Nome*: ", "Cognome*: ", "Provenienza*: ", "Indirizzo*: ",
                "Data di Nascita*: ", "Sesso*: " };
        JPanel personPanel = new JPanel();
        personPanel.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        personPanel.setLayout(new GridBagLayout());
        personGrid.fill = GridBagConstraints.HORIZONTAL;
        evenRows = isEven(iLabelName.length);
        for (int i = 0; i < rows; i++) {
            personGrid.gridy++;

            for (int j = 0; j < col; j++) {
                personGrid.gridx = j;
                personGrid.gridheight = 1;
                personGrid.gridwidth = 1;
                personGrid.weightx = 0.5;
                if (isEven(i) && index < iLabelName.length) {
                    if(iLabelName[index].equals("Data di Nascita*: ")) {
                        isDate=true;
                    }
                    JLabel jl = new JLabel("" + iLabelName[index]);
                    jl.setFont(labelFont);
                    personPanel.add(jl, personGrid);
                    index++;
                } else {
                    if (!evenRows && j > 0 && index >= iLabelName.length) {
                        break;
                    }
                    else {
                        if(!isDate || j==0) {
                            personPanel.add(new JTextField(), personGrid);
                        }
                        else {
                            if(j==1) {
                                personPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()),personGrid);
                                isDate=false;
                            }
                        }

                    }
                }
            }
        }

        return personPanel;
    }

    private JPanel psycGUI() {

        final JPanel psycPanel=new JPanel();
        GridBagConstraints psycGrid = new GridBagConstraints();
        psycPanel.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        psycPanel.setLayout(new GridBagLayout());
        psycGrid.fill = GridBagConstraints.HORIZONTAL;
        psycGrid.gridheight = 1;
        psycGrid.gridwidth = 1;
        psycGrid.weightx = 0.5;
        psycGrid.gridx = 0;
        psycGrid.gridy++;
        JLabel jl = new JLabel("" +"CF*: ");
        jl.setFont(labelFont);
        psycPanel.add(jl, psycGrid);
        psycGrid.gridx = 1;
        JLabel jl1 = new JLabel("" +"Nome*: ");
        jl.setFont(labelFont);
        psycPanel.add(jl1, psycGrid);
        psycGrid.gridy++;
        psycGrid.gridx = 0;
        psycPanel.add(new JTextField(), psycGrid);
        psycGrid.gridx = 1;
        psycPanel.add(new JTextField(), psycGrid);
        psycGrid.gridy++;
        psycGrid.gridx = 0;
        JLabel jl2 = new JLabel("" +"Cognome*: ");
        jl.setFont(labelFont);
        psycPanel.add(jl2, psycGrid);
        psycGrid.gridx = 1;
        JLabel jl3 = new JLabel("" +"Provenienza*: ");
        jl.setFont(labelFont);
        psycPanel.add(jl3, psycGrid);
        psycGrid.gridy++;
        psycGrid.gridx = 0;
        psycPanel.add(new JTextField(), psycGrid);
        psycGrid.gridx = 1;
        psycPanel.add(new JTextField(), psycGrid);
        psycGrid.gridy++;
        psycGrid.gridx = 0;
        JLabel jl6 = new JLabel("" +"Sesso*: ");
        jl.setFont(labelFont);
        psycPanel.add(jl6, psycGrid);    
        //        JLabel jl4 = new JLabel("" +"Indirizzo*: ");
        //        jl.setFont(labelFont);
        //        psycPanel.add(jl4, psycGrid);
        psycGrid.gridx = 1;
        JLabel jl5 = new JLabel("" +"Data di Nascita*: ");
        jl.setFont(labelFont);
        psycPanel.add(jl5, psycGrid);
        psycGrid.gridy++;
        psycGrid.gridx = 0;
        psycPanel.add(new JTextField(), psycGrid);
        //        psycPanel.add(new JTextField(), psycGrid);
        psycGrid.gridx = 1;
        psycPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), psycGrid);
        psycGrid.gridy++;
        psycGrid.gridx = 0;
        //        JLabel jl6 = new JLabel("" +"Sesso*: ");
        //        jl.setFont(labelFont);
        //        psycPanel.add(jl6, psycGrid);
        //        psycGrid.gridy++;
        //        psycGrid.gridx = 0;
        //        psycPanel.add(new JTextField(), psycGrid);
        psycGrid.gridy++;
        psycGrid.gridx=0;
        psycGrid.gridwidth=2;
        JButton jb=new JButton("Inserisci psichiatra");
        jb.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> list = getTextF(psycPanel);
                if(checkJPanel(list)) {
                    JOptionPane.showMessageDialog(psycPanel, "Parametri mancanti");
                }
                else {
                    String res = PsichiatriTable.getTable().persist(new Psichiatra((String)list.get(0), (String)list.get(1), 
                            (String)list.get(2), (String)(list.get(3)), (String)list.get(5),
                            (String)(list.get(4))));
                    JOptionPane.showMessageDialog(psycPanel, res);
                }               
                updateTable();
                SwingUtilities.updateComponentTreeUI(psycPanel);
            }
        });

        psycPanel.add(jb, psycGrid);
        return psycPanel;

    }


    private JPanel patientGUI() {
        final JPanel patPanel=new JPanel();
        GridBagConstraints patGrid = new GridBagConstraints();
        patPanel.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        patPanel.setLayout(new GridBagLayout());
        patGrid.fill = GridBagConstraints.HORIZONTAL;
        patGrid.gridheight = 1;
        patGrid.gridwidth = 1;
        patGrid.weightx = 0.5;
        patGrid.gridx = 0;
        patGrid.gridy++;
        JLabel jl = new JLabel("" +"CF*: ");
        jl.setFont(labelFont);
        patPanel.add(jl, patGrid);
        patGrid.gridx = 1;
        JLabel jl1 = new JLabel("" +"Nome*: ");
        jl.setFont(labelFont);
        patPanel.add(jl1, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        JLabel jl2 = new JLabel("" +"Cognome*: ");
        jl.setFont(labelFont);
        patPanel.add(jl2, patGrid);
        patGrid.gridx = 1;
        JLabel jl3 = new JLabel("" +"Provenienza*: ");
        jl.setFont(labelFont);
        patPanel.add(jl3, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        JLabel jl4 = new JLabel("" +"Indirizzo*: ");
        jl.setFont(labelFont);
        patPanel.add(jl4, patGrid);
        patGrid.gridx = 1;
        JLabel jl5 = new JLabel("" +"Data di Nascita*: ");
        jl.setFont(labelFont);
        patPanel.add(jl5, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        JLabel jl6 = new JLabel("" +"Sesso*: ");
        jl.setFont(labelFont);
        patPanel.add(jl6, patGrid);
        patGrid.gridx = 1;
        JLabel jl7 = new JLabel("" +"Data Arrivo*: ");
        jl.setFont(labelFont);
        patPanel.add(jl7, patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        patPanel.add(new JTextField(),patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        JLabel jl8 = new JLabel("" +"Data Rilascio*: ");
        jl.setFont(labelFont);
        patPanel.add(jl8, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), patGrid);
        patGrid.gridy++;

        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb=new JButton("Inserisci paziente");
        patPanel.add(jb, patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb1=new JButton("Inserisci visita");
        patPanel.add(jb1, patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb2=new JButton("Inserisci isolamento");
        patPanel.add(jb2, patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb3=new JButton("Inserisci cura");
        patPanel.add(jb3, patGrid);
        return patPanel;
    }

    private JPanel guardGUI() {

        final JPanel guardPanel=new JPanel();
        GridBagConstraints guardGrid = new GridBagConstraints();
        guardPanel.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        guardPanel.setLayout(new GridBagLayout());
        guardGrid.fill = GridBagConstraints.HORIZONTAL;
        guardGrid.gridheight = 1;
        guardGrid.gridwidth = 1;
        guardGrid.weightx = 0.5;
        guardGrid.gridx = 0;
        guardGrid.gridy++;
        JLabel jl = new JLabel("" +"CF*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl, guardGrid);
        guardGrid.gridx = 1;
        JLabel jl1 = new JLabel("" +"Nome*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl1, guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx = 0;
        guardPanel.add(new JTextField(), guardGrid);
        guardGrid.gridx = 1;
        guardPanel.add(new JTextField(), guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx = 0;
        JLabel jl2 = new JLabel("" +"Cognome*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl2, guardGrid);
        guardGrid.gridx = 1;
        JLabel jl3 = new JLabel("" +"Provenienza*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl3, guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx = 0;
        guardPanel.add(new JTextField(), guardGrid);
        guardGrid.gridx = 1;
        guardPanel.add(new JTextField(), guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx = 0;
        JLabel jl4 = new JLabel("" +"Indirizzo*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl4, guardGrid);
        guardGrid.gridx = 1;
        JLabel jl5 = new JLabel("" +"Data di Nascita*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl5, guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx = 0;
        guardPanel.add(new JTextField(), guardGrid);
        guardGrid.gridx = 1;
        guardPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx = 0;
        JLabel jl6 = new JLabel("" +"Sesso*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl6, guardGrid);
        guardGrid.gridx = 1;
        JLabel jl7 = new JLabel("" +"Data Arrivo*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl7, guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx=0;
        guardPanel.add(new JTextField(),guardGrid);
        guardGrid.gridx = 1;
        guardPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx=0;
        JLabel jl8 = new JLabel("" +"Data Rilascio*: ");
        jl.setFont(labelFont);
        guardPanel.add(jl8, guardGrid);
        guardGrid.gridy++;
        guardGrid.gridx = 0;
        guardPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), guardGrid);
        guardGrid.gridy++;

        guardGrid.gridx=0;
        guardGrid.gridwidth=2;
        JButton jb=new JButton("Inserisci guardia");
        guardPanel.add(jb, guardGrid);
        return guardPanel;
    }
    private JPanel attrezzatureGUI() {
        final JPanel attGUI=new JPanel();
        GridBagConstraints attGrid = new GridBagConstraints();
        attGUI.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        attGUI.setLayout(new GridBagLayout());
        attGrid.fill = GridBagConstraints.HORIZONTAL;
        attGrid.gridheight = 1;
        attGrid.gridwidth = 1;
        attGrid.weightx = 0.5;
        attGrid.gridx = 0;
        attGrid.gridy++;
        JLabel jl = new JLabel("" +"Descrizione*: ");
        jl.setFont(labelFont);
        attGUI.add(jl, attGrid);
        attGrid.gridx = 1;
        JLabel jl1 = new JLabel("" +"Costo*: ");
        jl.setFont(labelFont);
        attGUI.add(jl1, attGrid);
        attGrid.gridy++;
        attGrid.gridx = 0;
        attGUI.add(new JTextField(), attGrid);
        attGrid.gridx = 1;
        attGUI.add(new JTextField(), attGrid);
        attGrid.gridy++;
        attGrid.gridx = 0;
        attGrid.gridwidth=2;
        JButton jb=new JButton("Inserisci attrezzatura");
        attGUI.add(jb, attGrid);
        return attGUI;
    }
    private JPanel medicineGUI() {
        final JPanel medPanel=new JPanel();
        GridBagConstraints medGrid = new GridBagConstraints();
        medPanel.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        medPanel.setLayout(new GridBagLayout());
        medGrid.fill = GridBagConstraints.HORIZONTAL;
        medGrid.gridheight = 1;
        medGrid.gridwidth = 1;
        medGrid.weightx = 0.5;
        medGrid.gridx = 0;
        medGrid.gridy++;
        JLabel jl = new JLabel("" +"Tipo*: ");
        jl.setFont(labelFont);
        medPanel.add(jl, medGrid);
        medGrid.gridx = 1;
        JLabel jl1 = new JLabel("" +"Descrizione*: ");
        jl.setFont(labelFont);
        medPanel.add(jl1, medGrid);
        medGrid.gridy++;
        medGrid.gridx = 0;
        medPanel.add(new JTextField(), medGrid);
        medGrid.gridx = 1;
        medPanel.add(new JTextField(), medGrid);
        medGrid.gridy++;
        medGrid.gridx = 0;
        JLabel jl2 = new JLabel("" +"Data Scadenza*: ");
        jl.setFont(labelFont);
        medPanel.add(jl2, medGrid);
        medGrid.gridx = 1;
        JLabel jl3 = new JLabel("" +"Numero Scorte*: ");
        jl.setFont(labelFont);
        medPanel.add(jl3, medGrid);
        medGrid.gridy++;
        medGrid.gridx = 0;
        medPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), medGrid);
        medGrid.gridx = 1;
        medPanel.add(new JTextField(), medGrid);
        medGrid.gridy++;
        medGrid.gridx=0;
        medGrid.gridwidth=2;
        JButton jb=new JButton("Inserisci medicinale");
        medPanel.add(jb, medGrid);
        return medPanel;
    }
    
    private JPanel defaultGUI(String [] attributes, int rows, int col) {
        boolean evenRows;
        int index=0;


        JPanel medJP =new JPanel();
        GridBagConstraints medGrid = new GridBagConstraints();
        medJP.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        medJP.setLayout(new GridBagLayout());
        medGrid.fill = GridBagConstraints.HORIZONTAL;
        evenRows = isEven(attributes.length);
        for (int i = 0; i < rows; i++) {
            medGrid.gridy++;
            for (int j = 0; j < col; j++) {
                medGrid.gridx = j;
                medGrid.gridheight = 1;
                medGrid.gridwidth = 1;
                medGrid.weightx = 0.5;
                if (isEven(i) && index < attributes.length) {
                    JLabel jl = new JLabel("" + attributes[index]);
                    jl.setFont(labelFont);
                    medJP.add(jl, medGrid);
                    index++;
                } else {
                    if (!evenRows && j > 0 && index >= attributes.length) {
                        break;
                    }
                    medJP.add(new JTextField(), medGrid);
                }
            }
        }
        return medJP;

    }

    private void buildGrid(JPanel p, String[] l, int rows, int cols) {
        final boolean evenRows;

        int index = 0;
        final int resetX = personGrid.gridx;
        final int resetY = personGrid.gridy;
        evenRows = isEven(l.length);
        for (int i = 0; i < rows; i++) {
            personGrid.gridy++;
            for (int j = 0; j < cols; j++) {
                personGrid.gridx = j;
                if (isEven(i) && index < l.length) {

                    JLabel jl = new JLabel("" + l[index]);
                    jl.setFont(labelFont);
                    p.add(jl, personGrid);
                } else {
                    if (!evenRows && j > 0 && index >= l.length) {
                        break;
                    }
                    p.add(new JTextField(), personGrid);


                }
                index++;
            }
        }
        personGrid.gridx = resetX;
        personGrid.gridy = resetY;
    }

    private boolean isEven(int num) {
        return num % 2 == 0;
    }

    private JPanel visualizeTable(String nameEntity) {

        Pair<Vector, Vector> tableData = null;
       
        switch (nameEntity) {
        case "PSICHIATRI":
            tableData = PsichiatriTable.getTable().findAll();
            break;
        case "PAZIENTI":
            tableData = PazientiTable.getTable().findAll();
            break;
        case "MEDICINALI":
            tableData = MedicinaliTable.getTable().findAll();
            break;
        case "ATTREZZATURE":
            tableData = AttrezzatureTable.getTable().findAll();
            break;
        case "GUARDIE":
            tableData = GuardieTable.getTable().findAll();
            break;
        case "STANZE":
            tableData = StanzeTable.getTable().findAll();
        case "AREE":
            tableData = AreeTable.getTable().findAll();
        default:
            break;
        }
        table = new JTable(tableData.getY(), tableData.getX());
        updateTable();
        JPanel panel = new JPanel();      
        JScrollPane jsp = new JScrollPane(table);
        panel.setLayout(new BorderLayout());
        panel.add(jsp, BorderLayout.CENTER);

        return panel;
    }

    private void updateTable() {
//        table = VisiteTable.getTable().findAll();
//        DefaultTableModel model = (DefaultTableModel) table.getModel();
//        Vector row = (Vector) tableData.getY().get(tableData.getY().size()-1);
//        model.addRow(row);            
    }
    
    private void insertInto(String tabella) {
        List<String> list = getTextF((JPanel) centralPanel.getComponent(0));
        switch (tabella) {
        case "PSICHIATRI":
            if(checkJPanel(list)) {
                JOptionPane.showMessageDialog(this, "Parametri mancanti");
                break;
            }
            //            Psichiatra ps = new Psichiatra((String) list.get(0), (String) list.get(1), (String) list.get(2),
            //                    (String) list.get(3), (String) list.get(4), Date.valueOf(list.get(5)), (String) list.get(6),
            //                    Optional.ofNullable((String) list.get(7)), Optional.ofNullable((String) list.get(8)));
            //            ps.setSpecialization((String) list.get(9));
            // inserimento nel db di uno psichiatra
            //   String res = PsichiatriTable.getTable().persist();
            // JOptionPane.showMessageDialog(this, res);
            // sqlDriver.insertPsichiatra(ps.getSpecialization(),ps.getCF());
            break;
        case "PAZIENTI":// non che nomi hai dato ava

        default:
            break;
        }
    }

    private boolean checkJPanel(List<String> list) {
        return list.stream().anyMatch(i -> i.isEmpty());       
    }

}
