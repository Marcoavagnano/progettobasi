package graphicLayer;

import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.database.PsichiatriTable;

public class LoginPsichiatri {
   public LoginPsichiatri() {
	   final JFrame frame;
       final JPanel panel = new JPanel();
       final JLabel lblId = new JLabel("CodicePsichiatra");
       final JComboBox<String> comboPsichitri = new JComboBox<>(PsichiatriTable.getTable().getPsichiatri());
       frame = new JFrame("Psichiatri");
       frame.setBounds(100, 100, 450, 300);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       lblId.setBounds(154, 90, 46, 14);
       frame.getContentPane().add(lblId);


       final JButton btnLogin = new JButton("Login");
       
       btnLogin.addActionListener(new ActionListener() {

           @Override
           public void actionPerformed(ActionEvent e) {
               EventQueue.invokeLater(() -> new PsichiatraWindow(comboPsichitri.getSelectedItem()));
           }
       });

       comboPsichitri.addActionListener(new ActionListener() {

           @Override
           public void actionPerformed(ActionEvent e) {

           }
       });

       btnLogin.setBounds(183, 146, 89, 23);
       panel.add(lblId);
       panel.add(comboPsichitri);
       panel.add(btnLogin);
       frame.getContentPane().add(panel);
       frame.setVisible(true);
}

     

    
}