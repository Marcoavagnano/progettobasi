package graphicLayer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import ava.DateLabelFormatter;
import entity.Paziente;
import model.database.PazientiTable;
import model.database.StanzeTable;
import utils.Pair;

public class PsichiatraFinestra extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    final Font labelFont = new Font("", Font.BOLD, 12);
    JDatePanelImpl datePanel;
    String codPsichiatra;
    Pair<Vector, Vector> tableData = null;
    public PsichiatraFinestra(Object obj) {
        super(String.valueOf(obj));
        JPanel pannello = new JPanel(new GridLayout(2, 1));
        this.setSize(700, 700);
         codPsichiatra = String.valueOf(obj);
        UtilDateModel model = new UtilDateModel();
        Properties prop = new Properties();             
        prop.put("text.today", "Today");
        prop.put("text.month", "Month");
        prop.put("text.year", "Year");
        datePanel = new JDatePanelImpl(model, prop);
        final JPanel patPanel=new JPanel();
        GridBagConstraints patGrid = new GridBagConstraints();
        patPanel.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, 160));
        patPanel.setLayout(new GridBagLayout());
        patGrid.fill = GridBagConstraints.HORIZONTAL;   
        patGrid.gridheight = 1;
        patGrid.gridwidth = 1;
        patGrid.weightx = 0.5;
        patGrid.gridx = 0;
        patGrid.gridy++;
        JLabel jl = new JLabel("" +"CF*: ");
        jl.setFont(labelFont);
        patPanel.add(jl, patGrid);
        patGrid.gridx = 1;
        JLabel jl1 = new JLabel("" +"Nome*: ");
        jl.setFont(labelFont);
        patPanel.add(jl1, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        JLabel jl2 = new JLabel("" +"Cognome*: ");
        jl.setFont(labelFont);
        patPanel.add(jl2, patGrid);
        patGrid.gridx = 1;
        JLabel jl3 = new JLabel("" +"Provenienza*: ");
        jl.setFont(labelFont);
        patPanel.add(jl3, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        JLabel jl4 = new JLabel("" +"Indirizzo*: ");
        jl.setFont(labelFont);
        patPanel.add(jl4, patGrid);
        patGrid.gridx = 1;
        JLabel jl5 = new JLabel("" +"Data di Nascita*: ");
        jl.setFont(labelFont);
        patPanel.add(jl5, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JTextField(), patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        JLabel jl6 = new JLabel("" +"Sesso*: ");
        jl.setFont(labelFont);
        patPanel.add(jl6, patGrid);
        patGrid.gridx = 1;
        JLabel jl7 = new JLabel("" +"Data Arrivo*: ");
        jl.setFont(labelFont);
        patPanel.add(jl7, patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        patPanel.add(new JTextField(),patGrid);
        patGrid.gridx = 1;
        patPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        JLabel jl8 = new JLabel("" +"Data Rilascio*: ");
        jl.setFont(labelFont);
        patPanel.add(jl8, patGrid);
        patGrid.gridx=1;
        JLabel jl9 = new JLabel("" +"Codice Stanza*: ");
        jl.setFont(labelFont);
        patPanel.add(jl9, patGrid);
        patGrid.gridy++;
        patGrid.gridx = 0;
        patPanel.add(new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), prop), new DateLabelFormatter()), patGrid);
        patGrid.gridx = 1;
        JComboBox<String> stanzaLibere = new JComboBox<String>(StanzeTable.getTable().getStanzeLibere());
        patPanel.add(stanzaLibere,patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb=new JButton("Inserisci paziente");
        patPanel.add(jb, patGrid);
        jb.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> list = getTextF(patPanel);
                if(checkJPanel(list)) {
                    JOptionPane.showMessageDialog(patPanel, "Parametri mancanti");
                }
                else {
                    String res = PazientiTable.getTable().persist(new Paziente((String)list.get(0), (String)list.get(1), 
                            (String)list.get(2), (String)(list.get(5)), (String)list.get(6),
                            (String)(list.get(7)), Optional.ofNullable((String)(list.get(8))), 
                            (String)list.get(4), Integer.valueOf((String) stanzaLibere.getSelectedItem()),Integer.valueOf(codPsichiatra)));
                    JOptionPane.showMessageDialog(patPanel, res);
                }               
                updateTable();
                SwingUtilities.updateComponentTreeUI(patPanel);
                
            }
        });
        patGrid.gridy++;
        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb1=new JButton("Inserisci visita");
        patPanel.add(jb1, patGrid);
        jb1.addActionListener(new ActionListener() {    
            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(() -> new VisitaView(codPsichiatra));
            }
        });
        patGrid.gridy++;
        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb2=new JButton("Inserisci isolamento");
        jb2.addActionListener(new ActionListener() {    
            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(() -> new IsolamentoView(codPsichiatra));
            }
        });
        patPanel.add(jb2, patGrid);
        patGrid.gridy++;
        patGrid.gridx=0;
        patGrid.gridwidth=2;
        JButton jb3=new JButton("Inserisci cura");
        jb3.addActionListener(new ActionListener() {    
            @Override
            public void actionPerformed(ActionEvent e) {
                EventQueue.invokeLater(() -> new CuraView(codPsichiatra));
            }
        });
        patPanel.add(jb3, patGrid);
        tableData = PazientiTable.getTable().findByPsychiatric(Integer.valueOf(codPsichiatra));
        JPanel panel = new JPanel();
        JTable table = new JTable(tableData.getY(), tableData.getX());
        JScrollPane jsp = new JScrollPane(table);
        panel.setLayout(new BorderLayout());
        panel.add(jsp, BorderLayout.CENTER);
        pannello.add(patPanel);
        pannello.add(panel);
        this.getContentPane().add(pannello);
        this.setVisible(true);
    }

    private void updateTable() {
        tableData = PazientiTable.getTable().findByPsychiatric(Integer.valueOf(codPsichiatra));
    }
    
    private List<String> getTextF(Container container) {
        List<String> temp = new ArrayList<>();
        for (Component comp : container.getComponents()) {
            if (comp instanceof JTextField) {
                temp.add(((JTextField) comp).getText());
            }
            else if(comp instanceof JDatePickerImpl) {
                temp.add(((JDatePickerImpl) comp).getJFormattedTextField().getText());
            }
        }
        return temp;
    }

    private boolean checkJPanel(List<String> list) {
        return list.stream().anyMatch(i -> i.isEmpty());       
    }
}
