package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Vector;

import connection.DBConnection;
import entity.Isolamento;
import utils.Pair;

public class IsolamentiTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyIsolamentiTable {
        private static final IsolamentiTable SINGLETON = new IsolamentiTable();
    }

    private IsolamentiTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Isolamenti";
    }

    public static IsolamentiTable getTable() {
        return LazyIsolamentiTable.SINGLETON;
    }

    /**
     * Registra un isolamento nel DB.
     */

    public String persist(final Isolamento isolamento) {
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO isolamenti(CodicePaziente,\r\n" + 
                "       CodiceStanza,\r\n" + 
                "       Livello,\r\n" + 
                "       DataInizio,\r\n" + 
                "       DataFine) SELECT codicePaziente,?,?,?,? from pazienti where codicepaziente=?";

        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1,isolamento.getCodiceStanza());
            pStatement.setInt(2,isolamento.getLivello());
            pStatement.setDate(3,isolamento.getDataInizio());
            pStatement.setDate(4,isolamento.getDataFine().get());
            pStatement.setInt(5,isolamento.getCodicePaziente());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Isolamento inserito correttamente";
    }


    //trova isolamento dalle chiavi primarie ( da fare )
    public int findByPrimaryKey(final int CodiceMalattia) {
        Optional<Integer> codice = Optional.empty();
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where CodiceMalattia=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, CodiceMalattia);
            result = pStatement.executeQuery();
            if (result.next()) {
                codice = Optional.of(result.getInt("CodiceMalattia"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        if(codice.isPresent())
            return codice.get();
        else
            return -1;
    }

    public Pair<Vector, Vector> findByPsychiatric(final int codicePsichiatra) {
        String sql = "SELECT * FROM " + tableName + " WHERE codicePaziente IN "
                + "(SELECT codicePaziente FROM pazienti WHERE codicePsichiatra=" + codicePsichiatra + ")";
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}

