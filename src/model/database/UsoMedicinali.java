package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Vector;

import connection.DBConnection;
import utils.Pair;

public class UsoMedicinali {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyUsoMedicinali {
        private static final UsoMedicinali SINGLETON = new UsoMedicinali();
    }

    private UsoMedicinali() {
        this.dataSource = new DBConnection();
        this.tableName = "UsoMedicinali";
    }

    public static UsoMedicinali getTable() {
        return LazyUsoMedicinali.SINGLETON;
    }

    public String persist(final int codiceCura, final int codiceMedicinale) {
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO usoAttrezzature(codiceMedicinale,codiceCura) "
                + "VALUES(?,?)";
        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, codiceMedicinale);
            pStatement.setInt(2, codiceCura);
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "UsoMedicinale inserito correttamente";
    }


    public int findByPrimaryKey(final int CodiceMedicinale) {
        Optional<Integer> codiceMed = Optional.empty();
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where CodiceMedicinale=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, CodiceMedicinale);
            result = pStatement.executeQuery();
            if (result.next()) {
                codiceMed = Optional.of(result.getInt("CodiceMedicinale"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        if(codiceMed.isPresent())
            return codiceMed.get();
        else
            return -1;
    }

    public Pair<Vector, Vector> findMostUsed() {
        String sql = "SELECT CodiceMedicinale,Count(CodiceMedicinale) FROM " + tableName + " GROUP BY CodiceMedicinale ORDER BY codiceMedicinale DESC LIMIT 1";
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
    
    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}