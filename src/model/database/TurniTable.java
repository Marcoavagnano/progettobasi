package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import connection.DBConnection;
import entity.Turno;
import utils.Pair;

public class TurniTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyTurniTable {
        private static final TurniTable SINGLETON = new TurniTable();
    }

    private TurniTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Turni";
    }

    public static TurniTable getTable() {
        return LazyTurniTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Turno turno) {
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO turni(oraInizio,oraFine,data,codiceGuardia) "
                + "VALUES(?,?,?,?)";

        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setTime(1, turno.getOraInizio());
            pStatement.setTime(2, turno.getOraFine());
            pStatement.setDate(3, turno.getData());
            pStatement.setInt(4, turno.getCodiceGuardia());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Turno inserito correttamente";
    }


    public int findByPrimaryKey(final int CodiceStanza) {
        int codice = -1;
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where codicestanza=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, CodiceStanza);
            result = pStatement.executeQuery();
            if (result.next()) {
                codice = result.getInt("codiceStanza");
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return codice;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
    
    public Pair<Vector, Vector> findByGuardia(final int codiceGuardia) {
        String sql = "SELECT * FROM " + tableName + " Where codiceGuardia=" + codiceGuardia;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}
