//package model.database;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import connection.DBConnection;
//import entity.Anagrafica;
//
//public class AnagraficaTable {
//    private final DBConnection dataSource;
//    private final String tableName;
//
//    //Pattern singleton thread-safe.
//    private static class LazyAnagraficaTable {
//        private static final AnagraficaTable SINGLETON = new AnagraficaTable();
//    }
//
//    private AnagraficaTable() {
//        this.dataSource = new DBConnection();
//        this.tableName = "table";
//    }
//
//    public static AnagraficaTable getTable() {
//        return LazyAnagraficaTable.SINGLETON;
//    }
//
//    /**
//     * Registra un cliente nel DB.
//     * @param cliente il cliente da reistrare.
//     * @return il risultato dell'operazione compiuta.
//     */
//
//    public String persist(final Anagrafica persona) {
//        if (findByPrimaryKey(persona.getCF()) != null) {
//            System.out.println("Errore:" + "Person already exists");
//            return "Codice Fiscale Persona già esistente";
//        }
//        final Connection connection = this.dataSource.connect();
//        PreparedStatement pStatement = null;
//        String sql = "INSERT INTO anagrafica(CF,Nome,Cognome,Indirizzo,DataDiNascita,Email,Telefono,Sesso) "
//                + "VALUES(?,?,?,?,?,?,?,?)";
//
//        try {
//            pStatement = connection.prepareStatement(sql);
//            pStatement.setString(1, persona.getCF());
//            pStatement.setString(2, persona.getName());
//            pStatement.setString(3, persona.getSurname());
//            pStatement.setString(4, persona.getAddress());
//            pStatement.setDate(5, persona.getBirthDate());
//            if(persona.getEmail().isPresent())
//                pStatement.setString(6,  persona.getEmail().get());
//            if(persona.getTel().isPresent())
//                pStatement.setString(7, persona.getTel().get());
//            pStatement.setString(8, persona.getGender());
//            pStatement.executeUpdate();
//        } catch (SQLException e) {
//            new Exception(e.getMessage());
//            System.out.println(e.getMessage());
//            return "Errore di inserimento nel DB";
//        } finally {
//            try {
//                if (pStatement != null) {
//                    pStatement.close();
//                }
//                if (connection != null) {
//                    connection.close();
//                }
//            } catch (SQLException e) {
//                new Exception(e.getMessage());
//                System.out.println(e.getMessage());
//            }
//        }
//        return "Anagrafica inserita correttamente";
//    }
//
//
//    /**
//     * Ricerca un cliente per chiave primaria.
//     * @param idCliente l'id da ricercare.
//     * @return il cliente cercato se esiste, null altrimenti.
//     */
//    public String findByPrimaryKey(final String CF) {
//        String codiceFiscale = null;
//        final Connection connection = this.dataSource.connect();
//        PreparedStatement pStatement = null;
//        ResultSet result = null;
//
//        final  String query = "select * from " + this.tableName + " where CF=?";
//        try {
//            pStatement = connection.prepareStatement(query);
//            pStatement.setString(1, CF);
//            result = pStatement.executeQuery();
//            if (result.next()) {
//                codiceFiscale = result.getString("CF");
//            }
//        } catch (SQLException e) {
//            new Exception(e.getMessage());
//            System.out.println(e.getMessage());
//        } finally {
//            try {
//                if (pStatement != null) {
//                    pStatement.close();
//                }
//                if (connection != null) {
//                    connection.close();
//                }
//                if (result != null) {
//                    result.close();
//                }
//            } catch (SQLException e) {
//                new Exception(e.getMessage());
//                System.out.println(e.getMessage());
//            }
//        }
//        return codiceFiscale;
//    }
//}
