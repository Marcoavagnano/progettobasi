package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Vector;

import connection.DBConnection;
import entity.Attrezzatura;
import entity.Cura;
import utils.Pair;

public class CureTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyCureTable {
        private static final CureTable SINGLETON = new CureTable();
    }

    private CureTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Cure";
    }

    public static CureTable getTable() {
        return LazyCureTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Cura cura) {
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO cure(descrizione,pericolosita,durata,codicePsichiatra) "
                + "VALUES(?,?,?,?)";
        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, cura.getDescrizione());
            pStatement.setDouble(2, cura.getPericolosita());
            pStatement.setString(3, cura.getDurata());
            pStatement.setDouble(4, cura.getCodicePsichiatra());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Cura inserita correttamente";
    }


    public int findByPrimaryKey(final int codCura) {
        Optional<Integer> codiceMed = Optional.empty();
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where codiceCura=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, codCura);
            result = pStatement.executeQuery();
            if (result.next()) {
                codiceMed = Optional.of(result.getInt("codiceCura"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        if(codiceMed.isPresent())
            return codiceMed.get();
        else
            return -1;
    }

    public Pair<Vector, Vector> findByPsy(int codPsi) {
        String sql = "SELECT * FROM " + tableName + " where codicePsichiatra=" + codPsi;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
    
    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}
