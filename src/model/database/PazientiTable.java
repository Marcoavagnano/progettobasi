package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import connection.DBConnection;
import entity.Paziente;
import utils.Pair;

public class PazientiTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyPazientiTable {
        private static final PazientiTable SINGLETON = new PazientiTable();
    }

    private PazientiTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Pazienti";
    }

    public static PazientiTable getTable() {
        return LazyPazientiTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Paziente paziente) {
        if (findByPrimaryKey(findByPrimaryKey(paziente.getCF())) != null) {
            System.out.println("Errore:" + "Person already exists");
            return "Codice Fiscale Persona già esistente";
        }
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO pazienti(CF,\r\n" + 
                "       DataArrivo,\r\n" + 
                "       Nome,\r\n" + 
                "       Cognome,\r\n" + 
                "       DataDiNascita,\r\n" + 
                "       Provenienza,\r\n" + 
                "       Sesso,\r\n" + 
                "       CodiceStanza,\r\n" + 
                "       CodicePsichiatra) VALUES(?,?,?,?,?,?,?,?,?)";

        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, paziente.getCF());
            pStatement.setString(2, paziente.getArrivalDate());
            pStatement.setString(3, paziente.getName());
            pStatement.setString(4, paziente.getSurname());
            pStatement.setString(5, paziente.getBirthDate());
            pStatement.setString(6, paziente.getProvenienza());
            pStatement.setString(7, paziente.getGender());
            pStatement.setInt(8, paziente.getCodiceStanza());
            pStatement.setInt(9, paziente.getCodicePsichiatra());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Paziente inserito correttamente";
    }


    /**
     * Ricerca un cliente per chiave primaria.
     * @param idCliente l'id da ricercare.
     * @return il cliente cercato se esiste, null altrimenti.
     */
    public String findByPrimaryKey(final String codice) {
        String codiceFiscale = null;
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where codicePaziente=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, codice);
            result = pStatement.executeQuery();
            if (result.next()) {
                codiceFiscale = result.getString("codicePaziente");
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return codiceFiscale;
    }
    
    public String[] getPazienti(final int codPsichiatra) {
        List<String> pazientiList = new ArrayList<>();
        String sql = "SELECT codicePaziente FROM " + tableName + " Where codicePsichiatra=" + 1;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
           
            while(rs.next()) {
                pazientiList.add(String.valueOf(rs.getInt("codicePaziente")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }     
        String[] item = pazientiList.toArray(new String[pazientiList.size()]);  
        return item;
    }
    
    public String[] getPazienti() {
        List<String> pazientiList = new ArrayList<>();
        String sql = "SELECT codicePaziente FROM " + tableName;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
           
            while(rs.next()) {
                pazientiList.add(String.valueOf(rs.getInt("codicePaziente")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }     
        String[] item = pazientiList.toArray(new String[pazientiList.size()]);  
        return item;
    }

    public Pair<Vector, Vector> findByPsychiatric(final int codicePsichiatra) {
        String sql = "SELECT * FROM " + tableName + 
                " WHERE pazienti.codicePsichiatra =" + codicePsichiatra;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    if(!row.contains(rs.getString(i)))
                        row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    if(!row.contains(rs.getString(i)))
                        row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}
