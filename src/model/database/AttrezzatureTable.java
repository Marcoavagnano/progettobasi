package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Vector;

import connection.DBConnection;
import entity.Attrezzatura;
import utils.Pair;

public class AttrezzatureTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyAttrezzatureTable {
        private static final AttrezzatureTable SINGLETON = new AttrezzatureTable();
    }

    private AttrezzatureTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Attrezzature";
    }

    public static AttrezzatureTable getTable() {
        return LazyAttrezzatureTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Attrezzatura attrezzatura) {
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO attrezzature(descrizione,costo) "
                + "VALUES(?,?)";
        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, attrezzatura.getDescrizione());
            pStatement.setDouble(2, attrezzatura.getCosto());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Attrezzatura inserita correttamente";
    }


    public int findByPrimaryKey(final int CodiceAttrezzatura) {
        Optional<Integer> codiceMed = Optional.empty();
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where CodiceAttrezzatura=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, CodiceAttrezzatura);
            result = pStatement.executeQuery();
            if (result.next()) {
                codiceMed = Optional.of(result.getInt("CodiceAttrezzatura"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        if(codiceMed.isPresent())
            return codiceMed.get();
        else
            return -1;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}
