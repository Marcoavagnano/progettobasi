package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import connection.DBConnection;
import entity.Guardia;
import utils.Pair;


public class GuardieTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyGuardieTable {
        private static final GuardieTable SINGLETON = new GuardieTable();
    }

    private GuardieTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Guardie";
    }

    public static GuardieTable getTable() {
        return LazyGuardieTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Guardia guardia) {
        if (findByPrimaryKey(findByPrimaryKey(guardia.getCF())) != null) {
            System.out.println("Errore:" + "Person already exists");
            return "Codice Fiscale Persona già esistente";
        }
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO pazienti(CF,\r\n" + 
                "       Nome,\r\n" + 
                "       Cognome,\r\n" + 
                "       DataDiNascita,\r\n" + 
                "       Provenienza,\r\n" + 
                "       Sesso) VALUES(?,?,?,?,?,?)";

        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, guardia.getCF());
            pStatement.setString(2, guardia.getName());
            pStatement.setString(3, guardia.getSurname());
            pStatement.setDate(4, guardia.getBirthDate());
            pStatement.setString(5, guardia.getProvenienza());
            pStatement.setString(6, guardia.getGender());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Guardia inserito correttamente";
    }


    /**
     * Ricerca un cliente per chiave primaria.
     * @param idCliente l'id da ricercare.
     * @return il cliente cercato se esiste, null altrimenti.
     */
    public String findByPrimaryKey(final String CF) {
        String codiceFiscale = null;
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where CF=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, CF);
            result = pStatement.executeQuery();
            if (result.next()) {
                codiceFiscale = result.getString("CF");
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return codiceFiscale;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}
