package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Vector;
import connection.DBConnection;
import entity.Malattia;
import utils.Pair;

public class MalattieTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyMalattieTable {
        private static final MalattieTable SINGLETON = new MalattieTable();
    }

    private MalattieTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Malattie";
    }

    public static MalattieTable getTable() {
        return LazyMalattieTable.SINGLETON;
    }

    /**
     * Registra una malattia nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Malattia malattia) {
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO malattie(codiceMalattia,gravita,nome) "
                + "VALUES(?,?,?)";

        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, malattia.getCodiceMalattia());
            pStatement.setInt(2, malattia.getGravita());
            pStatement.setString(2, malattia.getNome());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Malattia inserita correttamente";
    }


    public int findByPrimaryKey(final int CodiceMalattia) {
        Optional<Integer> codice = Optional.empty();
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where CodiceMalattia=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, CodiceMalattia);
            result = pStatement.executeQuery();
            if (result.next()) {
                codice = Optional.of(result.getInt("CodiceMalattia"));
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        if(codice.isPresent())
            return codice.get();
        else
            return -1;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }
}
