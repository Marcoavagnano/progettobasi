package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.ComboBoxModel;

import connection.DBConnection;
import entity.Stanza;
import utils.Pair;

public class StanzeTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyStanzeTable {
        private static final StanzeTable SINGLETON = new StanzeTable();
    }

    private StanzeTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Stanze";
    }

    public static StanzeTable getTable() {
        return LazyStanzeTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Stanza stanza) {
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO stanze(codicestanza,settore,postioccupati,codiceArea) "
                + "VALUES(?,?,?,?)";

        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setInt(1, stanza.getCodiceStanza());
            pStatement.setInt(2, stanza.getSettore());
            pStatement.setInt(3, stanza.getPostiOccupati());
            pStatement.setInt(4, stanza.getCodiceArea());
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Stanza inserita correttamente";
    }


    public int findByPrimaryKey(final int CodiceStanza) {
        int codice = -1;
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where codicestanza=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setInt(1, CodiceStanza);
            result = pStatement.executeQuery();
            if (result.next()) {
                codice = result.getInt("codiceStanza");
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return codice;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName + 
                " inner join aree on aree.codicearea =" + tableName + ".codicearea";
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {
                if(!column.contains(rsmt.getColumnName(i)))
                    column.add(rsmt.getColumnName(i));
            }

            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {
                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }

    public String[] getStanzeLibere() {
        List<String> stanzeList = new ArrayList<>();
        String sql = "SELECT codicestanza FROM " + tableName + " where postioccupati<3";
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
           
            while(rs.next()) {
                stanzeList.add(String.valueOf(rs.getInt("codiceStanza")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }     
        String[] item = stanzeList.toArray(new String[stanzeList.size()]);  
        return item;
    }
}
