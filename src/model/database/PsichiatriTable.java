package model.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import connection.DBConnection;
import entity.Psichiatra;
import utils.Pair;

public class PsichiatriTable {
    private final DBConnection dataSource;
    private final String tableName;

    //Pattern singleton thread-safe.
    private static class LazyPsichiatriTable {
        private static final PsichiatriTable SINGLETON = new PsichiatriTable();
    }

    private PsichiatriTable() {
        this.dataSource = new DBConnection();
        this.tableName = "Psichiatri";
    }

    public static PsichiatriTable getTable() {
        return LazyPsichiatriTable.SINGLETON;
    }

    /**
     * Registra un cliente nel DB.
     * @param cliente il cliente da reistrare.
     * @return il risultato dell'operazione compiuta.
     */

    public String persist(final Psichiatra psichiatra) {
        if (findByPrimaryKey(findByPrimaryKey(psichiatra.getCF())) != null) {
            System.out.println("Errore:" + "Person already exists");
            return "Codice Fiscale Persona già esistente";
        }
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        String sql = "INSERT INTO psichiatri(CF,\r\n" + 
                "       Nome,\r\n" + 
                "       Cognome,\r\n" + 
                "       DataDiNascita,\r\n" + 
                "       Provenienza,\r\n" + 
                "       Sesso,\r\n" + 
                "       NumeroPazienti) VALUES(?,?,?,?,?,?,?)";

        try {
            pStatement = connection.prepareStatement(sql);
            pStatement.setString(1, psichiatra.getCF());
            pStatement.setString(2, psichiatra.getName());
            pStatement.setString(3, psichiatra.getSurname());
            pStatement.setString(4, psichiatra.getBirthDate());
            pStatement.setString(5, psichiatra.getProvenienza());
            pStatement.setString(6, psichiatra.getGender());
            pStatement.setInt(7, 0);
            pStatement.executeUpdate();
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
            return "Errore di inserimento nel DB";
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return "Psichiatra inserito correttamente";
    }


    /**
     * Ricerca un cliente per chiave primaria.
     * @param idCliente l'id da ricercare.
     * @return il cliente cercato se esiste, null altrimenti.
     */
    public String findByPrimaryKey(final String codicePsichiatra) {
        String codiceFiscale = null;
        final Connection connection = this.dataSource.connect();
        PreparedStatement pStatement = null;
        ResultSet result = null;

        final  String query = "select * from " + this.tableName + " where codicePsichiatra=?";
        try {
            pStatement = connection.prepareStatement(query);
            pStatement.setString(1, codicePsichiatra);
            result = pStatement.executeQuery();
            if (result.next()) {
                codiceFiscale = result.getString("codicePsichiatra");
            }
        } catch (SQLException e) {
            new Exception(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            try {
                if (pStatement != null) {
                    pStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println(e.getMessage());
            }
        }
        return codiceFiscale;
    }

    public Pair<Vector, Vector> findAll() {
        String sql = "SELECT * FROM " + tableName;
        Pair<Vector,Vector> dataTable;
        Vector data = new Vector();
        Vector row =new Vector();
        Vector column = null ;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){
            ResultSetMetaData rsmt  = rs.getMetaData();
            int c = rsmt.getColumnCount();
            column = new Vector(c);
            for (int i = 1; i <= c; i++) {

                column.add(rsmt.getColumnName(i));
            }
            while(rs.next()) {
                row = new  Vector(c);
                for (int i = 1; i <= c; i++) {

                    row.add(rs.getString(i));
                }
                data.add(row);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        dataTable = new Pair<Vector, Vector>(column, data);
        return dataTable;
    }

    public String[] getPsichiatri() {
        List<String> psichiatriList = new ArrayList<>();
        String sql = "SELECT CodicePsichiatra FROM " + tableName;
        try (Connection conn = dataSource.connect();
                Statement stmt  = conn.createStatement();
                ResultSet rs    = stmt.executeQuery(sql)){

            while(rs.next()) {
                psichiatriList.add(String.valueOf(rs.getInt("codicePsichiatra")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }     
        String[] item = psichiatriList.toArray(new String[psichiatriList.size()]);  
        return item;
    }
}