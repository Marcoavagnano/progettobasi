package connection;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author sqlitetutorial.net
 */
public class DBConnection {
    /**
     * Connect to a sample database
     */

    //db parameters
    static String url = "jdbc:sqlite:./res/ospedale.db";

    public Connection connect() {
        Connection conn = null;
        try {
            // create a connection to the database
            conn = DriverManager.getConnection(url);         
            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    
    public void insertIsolamento() {
        String sql = "INSERT INTO isolamenti(CodicePaziente,\r\n" + 
                "       CodiceStanza,\r\n" + 
                "       Livello,\r\n" + 
                "       DataInizio,\r\n" + 
                "       DataFine) VALUES(?,?,?,?,?)";

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, 100);
            pstmt.setInt(2, 234);
            pstmt.setInt(3, 1);
            pstmt.setDate(4, new Date(1));
            pstmt.setDate(5, new Date(2));
            
            pstmt.executeUpdate();
            System.out.println("Valori inseriti per isolamento");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void createNewTable() {
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS isolamenti (\n"
                + "     CodicePaziente integer NOT NULL,\n"
                + "     CodiceStanza integer NOT NULL,\n"
                + "     Livello integer NOT NULL,\n"
                + "     DataInizio date NOT NULL,\n"
                + "     DataFine date NOT NULL,\n"
                + "  PRIMARY KEY (CodicePaziente, CodiceStanza,DataInizio,DataFine), \r\n"
                + "  FOREIGN KEY (CodicePaziente) REFERENCES pazienti(CodicePaziente)"
                + "  FOREIGN KEY (CodiceStanza) REFERENCES stanze(CodiceStanza)"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            System.out.println("A new table has been created");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

//    static public void deletetable() {
//        String sql = "DROP TABLE psichiatri;";
//
//        try (Connection conn = DriverManager.getConnection(url);
//                Statement stmt = conn.createStatement()) {
//            // create a new table
//            stmt.execute(sql);
//            System.out.println("Table deleted");
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//    }  
//
//    public void insertAttrezzatura(Integer codiceAttrezzatura, String descrizione, Double costo) {
//        String sql = "INSERT INTO attrezzatura(CodiceAttrezzatura,Descrizione,Costo) "
//                + "VALUES(?,?,?)";
//
//        try (Connection conn = connect();
//                PreparedStatement pstmt = conn.prepareStatement(sql)) {
//            pstmt.setInt(1, codiceAttrezzatura);
//            pstmt.setString(2, descrizione);
//            pstmt.setDouble(3,costo);
//            pstmt.executeUpdate();
//            System.out.println("Valori Attrezzatura inseriti");
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//    }


    public void insertCura(int IdCura, String Descrizione, Integer Pericolosita,
            Date Durata, int IdPsichiatra) {
        String sql = "INSERT INTO cure(IdCura,\r\n" + 
                "       Descrizione,\r\n" + 
                "       Pericolosita,\r\n" + 
                "       Durata,\r\n" + 
                "       IdPsichiatra) VALUES(?,?,?,?,?)";

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, IdCura);
            pstmt.setString(2, Descrizione);
            pstmt.setInt(3, Pericolosita);
            pstmt.setDate(4, Durata);
            pstmt.setInt(5, IdPsichiatra);
            pstmt.executeUpdate();
            System.out.println("Valori inseriti per la cura");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


    public void insertPaziente(int CodicePaziente, Date DataArrivo, Date DataRilascio,
            int CF, int IdCura) {
        String sql = "INSERT INTO pazienti(CodicePaziente,\r\n" + 
                "       DataArrivo,\r\n" + 
                "       DataRilascio,\r\n" + 
                "       CF,\r\n" + 
                "       IdCura) VALUES(?,?,?,?,?)";

        try (Connection conn = connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, CodicePaziente);
            pstmt.setDate(2, DataArrivo);
            if(DataRilascio != null) 
                pstmt.setDate(3, DataRilascio);
            pstmt.setInt(4, CF);
            pstmt.setInt(5, IdCura);
            pstmt.executeUpdate();
            System.out.println("Valori inseriti per il paziente");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

//    static public ArrayList<Paziente> selectPatients() {
//        String sql = "SELECT id, name FROM patients";
//        ArrayList<Paziente> pazientiList = new ArrayList<>();
//        try (Connection conn = connect();
//                Statement stmt  = conn.createStatement();
//                ResultSet rs    = stmt.executeQuery(sql)){
//            // loop through the result set
//            while (rs.next()) {
//                pazientiList.add(new Paziente(rs.getInt(0), rs.getString(1)));
//            }
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }        
//        return pazientiList;
//    }
}