package entity;

public class Area {
    private int codiceArea;
    private String tipo;
    
    public Area(final int codiceArea, final String tipo) {
        this.codiceArea = codiceArea;
        this.tipo = tipo;
    }

    public int getCodiceArea() {
        return codiceArea;
    }

    public String getTipo() {
        return tipo;
    }
}
