package entity;

import java.sql.Date;

public class Psichiatra {
    final private String CF;
    final private String name;
    final private String surname;
    final private String birthDate;
    final private String gender;
    private String provenienza;


    public String getCF() {
        return CF;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getGender() {
        return gender;
    }


    public Psichiatra(final String CF, final String name,
            final String surname, final String provenienza,  final String birthDate,
            final String gender) {

        this.CF = CF;
        this.name = name;
        this.surname = surname;
        this.provenienza = provenienza;
        this.birthDate = birthDate;
        this.gender = gender;
    }

    public String getProvenienza() {
        return provenienza;
    }
}
