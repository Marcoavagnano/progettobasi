package entity;

public class Stanza {
    private Integer codiceStanza;
    private Integer settore;
    private Integer postiOccupati;
    private Integer codiceArea;

    public Stanza(final int codiceStanza,final int settore,final int postiOccupati,final int codiceArea) {
        this.codiceStanza = codiceStanza;
        this.settore = settore;
        this.postiOccupati = postiOccupati;
        this.codiceArea = codiceArea;
    }

    public Integer getCodiceStanza() {
        return codiceStanza;
    }

    public Integer getSettore() {
        return settore;
    }

    public Integer getPostiOccupati() {
        return postiOccupati;
    }

    public Integer getCodiceArea() {
        return codiceArea;
    }


}
