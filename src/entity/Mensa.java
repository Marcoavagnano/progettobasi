package entity;

public class Mensa {
    private Integer codiceMensa;
    private Integer numeroPazienti;
    private Integer postiTotali;
    
    public Mensa(final int codiceMensa,final int numeroPazienti,final int postiTotali) {
        this.codiceMensa = codiceMensa;
        this.numeroPazienti = numeroPazienti;
        this.postiTotali = postiTotali;
    }
    
    public Integer getCodiceMensa() {
        return codiceMensa;
    }
    public Integer getNumeroPazienti() {
        return numeroPazienti;
    }
    public Integer getPostiTotali() {
        return postiTotali;
    }
}
