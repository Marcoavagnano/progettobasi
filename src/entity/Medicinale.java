package entity;

import java.sql.Date;

public class Medicinale {
    final private String tipo;
    final private Date dataScadenza;
    final private String descrizione;
    final private int nScorte;

    public Medicinale(final String tipo,final Date dataScadenza,final String descrizione,final int nScorte) {
        this.tipo = tipo;
        this.dataScadenza = dataScadenza;
        this.descrizione = descrizione;
        this.nScorte = nScorte; 
    }

    public String getTipo() {
        return tipo;
    }

    public Date getDataScadenza() {
        return dataScadenza;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public int getnScorte() {
        return nScorte;
    }
}
