package entity;

public class Cura {
    final private String descrizione;
    final private Integer pericolosita;
    final private String durata;
    final private Integer codicePsichiatra;

    public Cura(final String descrizione,final Integer pericolosita, final String durata,final Integer codicePsichiatra) {
        this.descrizione = descrizione;
        this.pericolosita = pericolosita; 
        this.durata = durata; 
        this.codicePsichiatra = codicePsichiatra; 
    }

    public String getDescrizione() {
        return descrizione;
    }

    public Integer getPericolosita() {
        return pericolosita;
    }

    public String getDurata() {
        return durata;
    }

    public Integer getCodicePsichiatra() {
        return codicePsichiatra;
    }


}
