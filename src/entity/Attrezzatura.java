package entity;

public class Attrezzatura {
    final private String descrizione;
    final private double costo;

    public Attrezzatura(final String descrizione,final double costo) {
        this.descrizione = descrizione;
        this.costo = costo; 
    }

    public String getDescrizione() {
        return descrizione;
    }

    public double getCosto() {
        return costo;
    }

}
