package entity;

import java.sql.Date;
import java.util.Optional;

public class Paziente{

    private String CF;
    private String name;
    private String surname;
    private String birthDate;
    private String gender;
    private String arrivalDate;
    private String provenienza;
    private Optional<String> releaseDate;
    private Integer codicePsichiatra;
    private Integer codiceStanza;


    public Paziente(final String CF, final String name,
            final String surname, final String birthDate, final String gender,
            final String aDate, Optional<String> rDate, final String provenienza, final Integer codiceStanza,
            final Integer codicePsichiatra) {

        this.CF = CF;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.gender = gender;
        this.arrivalDate=aDate;
        this.releaseDate=rDate;
        this.provenienza = provenienza;
        this.codiceStanza = codiceStanza;
        this.codicePsichiatra = codicePsichiatra;

    }

    public String getProvenienza() {
        return provenienza;
    }
    
    public String getCF() {
        return CF;
    }


    public String getName() {
        return name;
    }


    public String getSurname() {
        return surname;
    }


    public String getBirthDate() {
        return birthDate;
    }


    public String getGender() {
        return gender;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }


    public Optional<String> getReleaseDate() {
        return releaseDate;
    }


    public Integer getCodicePsichiatra() {
        return codicePsichiatra;
    }


    public Integer getCodiceStanza() {
        return codiceStanza;
    }
}
