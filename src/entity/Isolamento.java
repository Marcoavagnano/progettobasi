package entity;

import java.sql.Date;
import java.util.Optional;

public class Isolamento {
	private Integer codicePaziente;
	private Integer codiceStanza;
	private Integer livello;
	private Date dataInizio;
	private Optional<Date> dataFine;
	
	public Isolamento(final Integer codicePaziente, final Integer codiceStanza,
			final Integer livello, final Date dataInizio, final Date dataFine) {
		this.codicePaziente = codicePaziente;
		this.codiceStanza = codiceStanza;
		this.livello = livello;
		this.dataInizio = dataInizio;
		this.dataFine = Optional.of(dataFine);
	}
	
	public Isolamento(final Integer codicePaziente, final Integer codiceStanza,
			final Integer livello, final Date dataInizio) {
		this.codicePaziente = codicePaziente;
		this.codiceStanza = codiceStanza;
		this.livello = livello;
		this.dataInizio = dataInizio;
		this.dataFine = Optional.empty();
	}

	public Integer getCodicePaziente() {
		return codicePaziente;
	}

	public Integer getCodiceStanza() {
		return codiceStanza;
	}

	public Integer getLivello() {
		return livello;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public Optional<Date> getDataFine() {
		return dataFine;
	}
}
