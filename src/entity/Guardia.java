package entity;

import java.sql.Date;

public class Guardia {

    private String tesserino;

    final private String CF;
    final private String name;
    final private String surname;
    final private Date birthDate;
    final private String gender;
    private String provenienza;
    private String specialization;

    public Guardia(final String CF, final String name,
            final String surname, final Date birthDate, final String gender,  final String provenienza,
            final String tesserino) {

        this.CF = CF;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.gender = gender;
        this.provenienza = provenienza;
        this.tesserino = tesserino;

    }
    
    public String getProvenienza() {
        return provenienza;
    }

    public void setTesserino(final String tesserino) {
        this.tesserino=tesserino;
    }
    public String getCF() {
        return CF;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getGender() {
        return gender;
    }

    public String getSpecialization() {
        return specialization;
    }

    public String getTesserino() {
        return tesserino;
    }

}
