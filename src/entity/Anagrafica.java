package entity;

import java.sql.Date;

public class Anagrafica {
    final private String CF;
    final private String name;
    final private String surname;
    final private Date birthDate;
    final private String gender;

    public Anagrafica(final String CF, final String name,
            final String surname, final String address,
            final Date birthDate, final String gender
            ) {
        this.CF=CF;
        this.name=name;
        this.surname=surname;
        this.birthDate=birthDate;
        this.gender=gender;
    }

    public String getCF() {
        return CF;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }


    public Date getBirthDate() {
        return birthDate;
    }

    public String getGender() {
        return gender;
    }


}
