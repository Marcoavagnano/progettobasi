package entity;

import java.sql.Date;
import java.sql.Time;

public class Turno {
    private Time oraInizio;
    private Time oraFine;
    private Date data;
    private Integer codiceGuardia;
    
    public Turno(final Time oraInizio, final Time oraFine,final Date data, final Integer codiceGuardia) {
       this.oraInizio = oraInizio;
       this.oraFine = oraFine;
       this.data = data;
       this.codiceGuardia = codiceGuardia;
    }

    public Time getOraInizio() {
        return oraInizio;
    }

    public Time getOraFine() {
        return oraFine;
    }

    public Date getData() {
        return data;
    }

    public Integer getCodiceGuardia() {
        return codiceGuardia;
    }
}
