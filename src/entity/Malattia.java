package entity;

public class Malattia {
    private Integer codiceMalattia;
    private Integer gravita;
    private String nome;
    
    public Malattia(final Integer codiceMalattia,final Integer gravita,final String nome) {
       this.codiceMalattia = codiceMalattia;
       this.gravita = gravita;
       this.nome = nome;
    }
    
    public Integer getCodiceMalattia() {
        return codiceMalattia;
    }
    public Integer getGravita() {
        return gravita;
    }
    public String getNome() {
        return nome;
    }
    
    
}
